package com.example.lotus.garudav3;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.lotus.garudav3.viewpager.ViewPagerCustomDuration;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RelativeLayout bookFlight;
    RelativeLayout myTrip;
    Intent intent;

    ImageView firstCircle;
    ImageView secondCircle;
    ImageView thirdCircle;

    ViewPagerCustomDuration viewPager;

    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 1000; // time in milliseconds between successive task executions.

    @Override
    protected void onStart() {
        super.onStart();
        /*After setting the adapter use the timer */
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            boolean forward = true;
            public void run() {
                if (currentPage == 2) {
                    forward = false;
                }else if(currentPage ==0){
                    forward = true;
                }
                Log.i("currentPage ", ""+currentPage);
                if(forward){
                    viewPager.setCurrentItem(currentPage++, true);
                }
                else{
                    viewPager.setCurrentItem(currentPage--, true);
                }
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String airport;
                    String newGate;
                    String oldGate;
                    try {
                        Log.d("socket", data.toString());
                        airport = data.getString("airport");
                        newGate = data.getString("gate");
                        oldGate = data.getString("oldGate");
                        callNotif(airport, newGate, oldGate);
                    } catch (JSONException e) {
                        Log.d("socket", e.toString());
                        return;
                    }
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSocket.on("change_gate", onNewMessage);
        mSocket.connect();

        firstCircle = (ImageView)findViewById(R.id.first_circle);
        secondCircle = (ImageView)findViewById(R.id.second_circle);
        thirdCircle = (ImageView)findViewById(R.id.third_circle);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTextAlignment();
        setSupportActionBar(toolbar);

        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        bookFlight = (RelativeLayout) findViewById(R.id.book_flight);
        myTrip = (RelativeLayout) findViewById(R.id.my_trip);

        bookFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timer.cancel();
                intent = new Intent(MainActivity.this, BookActivity.class);
                startActivity(intent);
            }
        });

        myTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sendPost();
            }
        });


        // For Slider in Main Activity
        viewPager = (ViewPagerCustomDuration) findViewById(R.id.main_pager);
        viewPager.setScrollDurationFactor(5); // make the animation five as slow
        viewPager.setAdapter(new CustomPagerAdapter(this));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            public void onPageSelected(int position) {
                currentPage = position;
                // Check if this is the page you want.
                changePromoFocus(position);
                // Toast.makeText(MainActivity.this, ""+position, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://cdn.makersinstitute.id");
        } catch (URISyntaxException e) {}
    }

    public void callNotif(String airport, String newGate, String oldGate){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logobw)
                        .setContentTitle("Message from Garuda")
                        .setContentText("There was a change in " + airport + " airport")
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, NotificationActivity.class);
        resultIntent.putExtra("message", "General Update");
        resultIntent.putExtra("subtitle", "Gate has changed from " + oldGate +" to " + newGate + " in " + airport + " airport");
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        //stackBuilder.addParentStack(BackgroundService.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("socket", "destroy");
        //mSocket.disconnect();
        //mSocket.off("change_gate", onNewMessage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent intent = new Intent(this, NotificationActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_tnc) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            alertDialogBuilder.setTitle("Legal Term");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Warning: This program is protected by copyright law and international treaties. Unauthorized reproduction or distribution of the program, or any portion, may result in civil and criminal penalties, and will be prosecuted to the maximum extent under the law.")
                    .setCancelable(false)
                    .setPositiveButton("Yes, I agree",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // Ini sebelum ada corousel dipakenya
    public class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;

        CustomPagerAdapter(Context context) {
            mContext = context;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ViewGroup layout = (ViewGroup) inflater.inflate(customPagerEnum.getLayoutResId(), collection, false);
            collection.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return CustomPagerEnum.values().length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
            return customPagerEnum.getTitleResId();
        }
    }

    public enum CustomPagerEnum {

        PROMOTE_01("Promote_01", R.layout.promote_01),
        PROMOTE_02("Promote_02", R.layout.promote_02),
        PROMOTE_03("Promote_03", R.layout.promote_03);

        private String mTitleResId;
        private int mLayoutResId;

        CustomPagerEnum(String titleResId, int layoutResId) {
            mTitleResId = titleResId;
            mLayoutResId = layoutResId;

        }

        public String getTitleResId() {
            return mTitleResId;
        }

        public int getLayoutResId() {
            return mLayoutResId;
        }

    }

    void changePromoFocus(int layoutPromote){
        switch (layoutPromote){
            case 0 :
                firstCircle.setImageResource(R.drawable.circle_white);
                secondCircle.setImageResource(R.drawable.circle_alpha);
                thirdCircle.setImageResource(R.drawable.circle_alpha);
                break;
            case 1 :
                firstCircle.setImageResource(R.drawable.circle_alpha);
                secondCircle.setImageResource(R.drawable.circle_white);
                thirdCircle.setImageResource(R.drawable.circle_alpha);
                break;
            case 2 :
                firstCircle.setImageResource(R.drawable.circle_alpha);
                secondCircle.setImageResource(R.drawable.circle_alpha);
                thirdCircle.setImageResource(R.drawable.circle_white);
                break;
        }
    }
}
